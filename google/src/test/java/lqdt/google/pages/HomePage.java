
package lqdt.google.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lqdt.google.common.*;
 
//Selenium Helper instance is already available as 'helper'

public class HomePage extends PageBase {
	
	//Page Elements - defined every selector on a page here
	
	@FindBy(how=How.NAME, using = "q")//can elect to leave this off and just use the variable name as the item name
	private WebElement txtSearchTerm;
	
	@FindBy(how=How.ID, using = "gbqfq")
	private WebElement btnGoogleSearch;
	
	//Dropdowns are Selects in selenium
	//private Select ddlSomething;//dropdown example
	
	

	//Page Methods

	public HomePage(WebDriver driver) {
		super(driver);//always include this in any page class
		 
		//verify proper page title
		//throw error if wrong page
		//logger.info("Not the home page");
		
	}

	//Selenium Method exercising search
	 public void GoogleSearch(String strSearchTerm)
	 { 
		 txtSearchTerm.sendKeys(strSearchTerm);
		 btnGoogleSearch.click();
		 
		// ddlSomething.selectByValue(strSearchTerm); //Dropdown select Example
		
	 }
	 
	 //Selenium method to click sign-in
	 public void SignIn()
	 {
		 
		 
	 }
	 

}
